var gulp = require('gulp')
    bower = require('gulp-bower');

var config = {
	bowerDir: './bower_components'
}

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
});

gulp.task('bootstrap', ['bower'], function() {
	gulp.src('./bower_components/bootstrap/dist/**/*')
		.pipe(gulp.dest('./public/assets/bootstrap'));
});

gulp.task('jquery', ['bower'], function() {
	gulp.src('./bower_components/jquery/dist/**/*')
		.pipe(gulp.dest('./public/assets/jquery'));
});

gulp.task('default', ['bower','bootstrap', 'jquery']);